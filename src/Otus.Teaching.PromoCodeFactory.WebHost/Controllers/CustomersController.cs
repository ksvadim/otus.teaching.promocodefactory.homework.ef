﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
       private readonly CustomerRepository _customerRepository;
       private readonly IRepository<Preference> _preferenceRepository;

       public CustomersController(CustomerRepository customerRepository,
          IRepository<Preference> preferenceRepository)
       {
          _customerRepository = customerRepository;
          _preferenceRepository = preferenceRepository;
       }
       
        /// <summary>
        /// Получает всех клиентов.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
           var customers = await _customerRepository
              .GetAllAsync();

           var customersResponse = customers
              .Select(x =>
                 new CustomerShortResponse
                 {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                 })
              .ToList();

           return Ok(customersResponse);
        }
        
        /// <summary>
        /// Получает клиента по ИД.
        /// </summary>
        /// <param name="id">ИД.</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
           var customer = await _customerRepository.GetByIdAsync(id);

           if (customer == null)
              return NotFound();

           var customerResponse = new CustomerResponse(customer);

           return Ok(customerResponse);
        }
        
        /// <summary>
        /// Создает нового клиента.
        /// </summary>
        /// <param name="request">Запрос на создание клиента.</param>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
           var customer = new Customer
           {
              Id = Guid.NewGuid(),
              FirstName = request.FirstName,
              LastName = request.LastName,
              Email = request.Email
           };

           var preferences = await _preferenceRepository.GetWhere(x => request.PreferenceIds.Contains(x.Id));

           customer.Preferences = preferences
              .Select(x =>
                 new CustomerPreference
                 {
                    CustomerId = customer.Id,
                    PreferenceId = x.Id
                 })
              .ToList();

           await _customerRepository.AddAsync(customer);

           return Ok();
        }

        /// <summary>
        /// Обновляет данные клиента.
        /// </summary>
        /// <param name="id">ИД.</param>
        /// <param name="request">Запрос на обновление данных.</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
           var customer = await _customerRepository.GetByIdAsync(id);

           if (customer == null)
              return NotFound();

           var preferences = await _preferenceRepository.GetWhere(x => request.PreferenceIds.Contains(x.Id));

           customer.Preferences = preferences
              .Select(x =>
                 new CustomerPreference
                 {
                    CustomerId = customer.Id,
                    PreferenceId = x.Id
                 })
              .ToList();

           customer.FirstName = request.FirstName;
           customer.LastName = request.LastName;
           customer.Email = request.Email;

           await _customerRepository.UpdateAsync(customer);

           return Ok();
        }

        /// <summary>
        /// Удаляет клиента по ИД.
        /// </summary>
        /// <param name="id">ИД.</param>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
           var customer = await _customerRepository.GetByIdAsync(id);
            
           if (customer == null)
              return NotFound();

           await _customerRepository.DeleteAsync(customer);

           return NoContent();
        }
    }
}