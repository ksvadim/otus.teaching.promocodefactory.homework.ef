﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public Guid Id { get; set; }
        
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public string PartnerName { get; set; }

        public PromoCodeShortResponse() { }

        public PromoCodeShortResponse(PromoCode promoCode)
        {
           Id = promoCode.Id;
           Code = promoCode.Code;
           ServiceInfo = promoCode.ServiceInfo;
           BeginDate = promoCode.BeginDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss");
           EndDate = promoCode.EndDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss");
           PartnerName = promoCode.PartnerName;
        }
    }
}